import { defineConfig, presetIcons, transformerDirectives, transformerVariantGroup } from 'unocss';
import { presetApplet, presetRemRpx, transformerApplet, transformerAttributify } from 'unocss-applet';

export default defineConfig({
  theme: {
    colors: {
      primary: 'rgb(var(--uni-parimary-color))',
    },
  },
  presets: [
    presetApplet(),
    presetRemRpx({
      baseFontSize: 2,
    }),
    presetIcons(),
  ],
  transformers: [transformerDirectives(), transformerVariantGroup(), transformerApplet(), transformerAttributify()],
});

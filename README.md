## uniapp-vue3-template

### 安装

```bash
pnpm install
```

### 打包与运行

开发环境和正式环境打包运行参考[uniapp 官方文档](https://uniapp.dcloud.net.cn/quickstart-cli.html)

### unocss

因为 unocss 在 0.59.0 版本以后仅支持 esm，目前@dcloudio/vite-plugin-uni 采用的是 commonJS 的方式。所以不能安装 0.59.0 版本以上的 unocss。
推荐搭配 UnoCSS 插件使用，提示 class 内容

### wot-design-uni

基于Vue3+TS开发，提供70+高质量组件，[官方文档](https://wot-design-uni.netlify.app)

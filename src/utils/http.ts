import { USER_TOKEN } from '@/constants/local-storage-key-const';
import { ACCESS_TOKEN_KEY } from '@/constants/index';
interface RequestParams {
  url: string;
  method?: 'GET' | 'POST' | 'PUT' | 'DELETE';
  data?: any;
  header?: any;
}
const baseUrl = import.meta.env.VITE_APP_URL;
function getUserToken() {
  const token = uni.getStorageSync(USER_TOKEN);
  if (token) {
    return token;
  }
  return '';
}

/**
 * 处理返回的消息
 */
function handleResponse(
  response: UniApp.RequestSuccessCallbackResult,
  resolve: (value: Recordable<any> | Recordable<any>[] | PromiseLike<Recordable<any> | Recordable<any>[]>) => void,
  reject: (reason?: any) => void
) {
  const { code, message, success, result } = response.data as Result;
  if (code === 401) {
    uni.showModal({
      title: '提示',
      content: '登录已失效，请重新登录',
      showCancel: false,
      success: (res) => {
        if (res.confirm) {
          uni.navigateTo({
            url: '/pages/login/index',
          });
        }
      },
    });
    reject(message);
  }
  if (code === 200 && success) {
    resolve(result);
  } else {
    uni.showToast({
      title: message,
      icon: 'error',
    });
    reject(message);
  }
}
/**
 * 通用请求封装
 */
export const request = ({ url, method, data, header }: RequestParams) => {
  return new Promise<Recordable | Recordable[]>((resolve, reject) => {
    uni.request({
      url: baseUrl + url, //拼接请求路径
      data: data,
      method: method || 'GET',
      header: {
        ...header,
        [ACCESS_TOKEN_KEY]: getUserToken(),
      },
      success: (response) => {
        handleResponse(response, resolve, reject);
      },
      fail: (error) => {
        reject(error);
      },
    });
  });
};

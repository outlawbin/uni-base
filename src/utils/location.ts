interface ILocation {
  longitude: number;
  latitude: number;
}
export function getLocation() {
  return new Promise<ILocation>((resolve, reject) => {
    uni.getLocation({
      type: 'wgs84',
      success: (res) => {
        resolve({
          longitude: res.longitude,
          latitude: res.latitude,
        });
      },
      fail: (error) => {
        if (error.errMsg === 'getLocation:fail auth deny') {
          reject(new Error('获取位置信息授权失败,请到设置-授权列表开启授权'));
        } else {
          reject(new Error('获取位置失败'));
        }
      },
    });
  });
}

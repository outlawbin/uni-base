import { request } from '@/utils/http';
export const userApi = {
  /**
   * 账户密码登录
   * @param data {username, password}
   * @returns 用户token和用户信息
   */
  accountLogin: (data: Recordable) => {
    return request({ url: '/mobile/common/mLogin', method: 'POST', data });
  },
  /**
   * 获取手机验证码
   * @param data {mobile}
   * @returns 验证码
   */
  getMobileCode: (data: Recordable) => {
    return request({ url: '/mobile/common/mobileSms', method: 'GET', data });
  },
};

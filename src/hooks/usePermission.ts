import { USER_PERMISSIONS } from '@/constants/local-storage-key-const';
export function usePermission() {
  /**
   * 判断是否拥有权限
   * @param permission 权限标识符
   * @param options 配置项{ and : false }，and为true时，表示所有权限都满足才返回true，默认为false
   * @returns boolean 满足返回true，否则返回false
   */
  function hasPermission(
    permission: string | string[],
    options?: {
      and: false;
    }
  ): boolean {
    const permissions = uni.getStorageSync(USER_PERMISSIONS) || [];
    const and = options?.and;
    if (Array.isArray(permission)) {
      // 如果and为true，则判断所有权限是否都满足
      if (and) {
        return permission.every((item) => permissions.includes(item));
      }
      // 如果and为false，则判断是否有任意一个权限满足
      return permission.some((item) => permissions.includes(item));
    }
    // 如果权限是字符串，则直接判断是否满足
    return permissions.includes(permission);
  }
  function setPermission(permissions: string[]) {
    uni.setStorageSync(USER_PERMISSIONS, permissions);
  }

  function clearPermission() {
    uni.removeStorageSync(USER_PERMISSIONS);
  }

  return {
    hasPermission,
    setPermission,
    clearPermission,
  };
}

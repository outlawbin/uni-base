import { userApi } from '@/api/user-api';
import { USER_TOKEN, USER_INFO } from '@/constants/local-storage-key-const';

export function useUser() {
  // 获取token
  function getToken(): string {
    const localToken = uni.getStorageSync(USER_TOKEN);
    return localToken || '';
  }
  // 获取用户信息
  function getUserInfo() {
    const localUserInfo = uni.getStorageSync(USER_INFO);
    return localUserInfo || null;
  }
  function success({ userInfo, token }: { userInfo: Recordable; token: string }) {
    uni.setStorageSync(USER_TOKEN, token);
    uni.setStorageSync(USER_INFO, userInfo);
  }
  // 账户密码登录
  async function accountLogin(data: Recordable) {
    const result = (await userApi.accountLogin(data)) as Recordable;
    success({ userInfo: result?.userInfo, token: result?.token });
  }
  // 退出登录
  function logout() {
    uni.removeStorageSync(USER_TOKEN);
    uni.removeStorageSync(USER_INFO);
    uni.reLaunch({
      url: '/pages/login/index',
    });
  }
  return {
    accountLogin,
    getToken,
    getUserInfo,
    logout,
  };
}

declare global {
  // 全局声明
  declare type Recordable<T = any> = Record<string, T>;
  // 网络请求结果
  declare interface Result<T = any> {
    code: number;
    message: string;
    success: boolean;
    result: T;
    timestamp: number;
  }
}
export {};

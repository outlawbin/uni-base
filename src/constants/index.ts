/**
 * 常量key名集合
 */

/**
 * 请求头Authorization的key名
 */
export const ACCESS_TOKEN_KEY = 'x-access-token';

/**
 *请求头的content-type
 */
export const CONTENT_TYPE = {
  JSON: 'application/json;charset=UTF-8',
  FORM: 'application/x-www-form-urlencoded',
};

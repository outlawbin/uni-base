/*
 *  key  常量
 */

/**
 * key前缀
 */
const KEY_PREFIX = 'uni_base_';
/**
 * localStorageKey集合
 */
// token
export const USER_TOKEN = `${KEY_PREFIX}token`;
// 账户密码account
export const USER_ACCOUNT = `${KEY_PREFIX}account`;
// 用户信息
export const USER_INFO = `${KEY_PREFIX}user`;
// 用户权限
export const USER_PERMISSIONS = `${KEY_PREFIX}permissions`;
